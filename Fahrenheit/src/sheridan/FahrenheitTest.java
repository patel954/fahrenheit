package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.JUnitCore;


 public class FahrenheitTest {

	@Test
	public void testConvertFromCelsius() {
	int f = Fahrenheit.convertFromCelsius(20);
	assertTrue("It will passess" , f == -6);


	}

	@Test
	public void testConvertFromCelsiusExceptional() {
	int f = Fahrenheit.convertFromCelsius(20);
	assertFalse("It will passess exception" , f != -6);
	}

	@Test
	public void testConvertFromCelsiusBoundryIn() {
	int f = Fahrenheit.convertFromCelsius(20);
	assertEquals(-6, f);
	}

	@Test
	public void testConvertFromCelsiusBoundryOut() {
	int f = Fahrenheit.convertFromCelsius(50);
	assertFalse("It will passess" , f == -6);
	}


}
